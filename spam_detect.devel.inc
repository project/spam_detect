<?php

/**
 * @file
 * Functionality to aid in the development of Spam Detect and related modules.
 */

/**
 * Spam detection test form.
 */
function spam_detect_devel_form() {
  $form = array();

  $form['test_content'] = array(
    '#type' => 'textarea',
    '#title' => t('Text to test'),
    '#description' => t('Enter the text to test the spam filters with.'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Check spamminess'),
  );

  $form['mark_as_spam'] = array(
    '#type' => 'submit',
    '#value' => t('Train filters: Spam'),
  );

  $form['mark_as_not_spam'] = array(
    '#type' => 'submit',
    '#value' => t('Train filters: Not Spam'),
  );

  return $form;
}

/**
 * Spam detection test form submission handler.
 *
 * If text should be flaggesd as spam or not spam, train filters accordingly;
 * otherwise, evaluate text for spamminess and print results.
 */
function spam_detect_devel_form_submit($form, &$form_state) {
  $string = $form_state['values']['test_content'];
  $form_state['rebuild'] = TRUE;
  if ($form_state['values']['op'] === $form_state['values']['mark_as_spam']) {
    spam_detect_train_filters($string, TRUE);
    drupal_set_message(t('Filters have been trained to see the submitted content as spam.'), 'status');
  }
  elseif ($form_state['values']['op'] === $form_state['values']['mark_as_not_spam']) {
    spam_detect_train_filters($string, FALSE);
    drupal_set_message(t('Filters have been trained to see the submitted content as not spam.'), 'status');
  }
  else {
    $info = spam_detect_calculate_spamminess($string, array());
    drupal_set_message('<pre>' . var_export($info, TRUE) . '</pre>', 'status');
    if (($info['certainty'] >= variable_get('spam_detect_spam_threshold', .75))) {
      drupal_set_message(t('Content would be considered spam.'), 'status');
    }
    else {
      drupal_set_message(t('Content would not be considered spam.'), 'status');
    }    
  }
}
