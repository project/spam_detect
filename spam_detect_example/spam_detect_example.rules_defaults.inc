<?php
/**
 * @file
 * spam_detect_example.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function spam_detect_example_default_rules_configuration() {
  $items = array();
  $items['rules_detect_spam'] = entity_import('rules_config', '{ "rules_detect_spam" : {
      "LABEL" : "Detect spam",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "spam_detect_rules", "rules", "flag" ],
      "ON" : { "node_insert" : [] },
      "IF" : [ { "text_is_spammy" : { "text" : [ "node:body:value" ] } } ],
      "DO" : [
        { "node_unpublish" : { "node" : [ "node" ] } },
        { "flag_flagnode" : {
            "flag" : "spam",
            "node" : [ "node" ],
            "flagging_user" : [ "node:author" ],
            "permission_check" : 1
          }
        }
      ]
    }
  }');
  $items['rules_link_condition__cloned'] = entity_import('rules_config', '{ "rules_link_condition__cloned" : {
      "LABEL" : "Rules link: Mark as spam (cloned) condition",
      "PLUGIN" : "and",
      "OWNER" : "rules",
      "USES VARIABLES" : { "node" : { "type" : "node", "label" : "node" } },
      "AND" : []
    }
  }');
  $items['rules_link_condition_mark_not_spam'] = entity_import('rules_config', '{ "rules_link_condition_mark_not_spam" : {
      "LABEL" : "Rules link: Mark as not spam condition",
      "PLUGIN" : "and",
      "OWNER" : "rules",
      "USES VARIABLES" : { "node" : { "type" : "node", "label" : "node" } },
      "AND" : []
    }
  }');
  $items['rules_link_condition_spam'] = entity_import('rules_config', '{ "rules_link_condition_spam" : {
      "LABEL" : "Rules link: Spam condition",
      "PLUGIN" : "and",
      "OWNER" : "rules",
      "USES VARIABLES" : { "node" : { "type" : "node", "label" : "node" } },
      "AND" : []
    }
  }');
  $items['rules_link_set_mark_not_spam'] = entity_import('rules_config', '{ "rules_link_set_mark_not_spam" : {
      "LABEL" : "Rules link: Mark as not spam rules set",
      "PLUGIN" : "rule set",
      "OWNER" : "rules",
      "REQUIRES" : [ "spam_detect_rules", "flag", "rules" ],
      "USES VARIABLES" : { "node" : { "type" : "node", "label" : "node" } },
      "RULES" : [
        { "RULE" : {
            "DO" : [
              { "spam_detect_rules_train_filters" : { "text" : [ "node:body:value" ], "is_spam" : 0 } },
              { "flag_unflagnode" : {
                  "flag" : "spam",
                  "node" : [ "node" ],
                  "flagging_user" : [ "node:author" ],
                  "permission_check" : 1
                }
              },
              { "node_publish" : { "node" : [ "node" ] } }
            ],
            "LABEL" : "Mark not spam"
          }
        }
      ]
    }
  }');
  $items['rules_link_set_spam'] = entity_import('rules_config', '{ "rules_link_set_spam" : {
      "LABEL" : "Rules link: Spam rules set",
      "PLUGIN" : "rule set",
      "OWNER" : "rules",
      "REQUIRES" : [ "spam_detect_rules", "rules" ],
      "USES VARIABLES" : { "node" : { "type" : "node", "label" : "node" } },
      "RULES" : [
        { "RULE" : {
            "DO" : [
              { "spam_detect_rules_train_filters" : { "text" : [ "node:body:value" ], "is_spam" : 1 } },
              { "entity_delete" : { "data" : [ "node" ] } }
            ],
            "LABEL" : "Mark spam"
          }
        }
      ]
    }
  }');
  return $items;
}
