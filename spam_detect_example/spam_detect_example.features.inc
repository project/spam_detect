<?php
/**
 * @file
 * spam_detect_example.features.inc
 */

/**
 * Implements hook_views_api().
 */
function spam_detect_example_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_flag_default_flags().
 */
function spam_detect_example_flag_default_flags() {
  $flags = array();
  // Exported flag: "Spam".
  $flags['spam'] = array(
    'entity_type' => 'node',
    'title' => 'Spam',
    'global' => 1,
    'types' => array(
      0 => 'article',
      1 => 'page',
    ),
    'flag_short' => 'Spam',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'Not spam',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'normal',
    'weight' => 0,
    'show_in_links' => array(
      'full' => 0,
      'teaser' => 0,
      'rss' => 0,
      'search_index' => 0,
      'search_result' => 0,
    ),
    'show_as_field' => 0,
    'show_on_form' => 0,
    'access_author' => '',
    'show_contextual_link' => 0,
    'i18n' => 0,
    'api_version' => 3,
    'module' => 'spam_detect_example',
    'locked' => array(
      0 => 'name',
    ),
  );
  return $flags;

}

/**
 * Implements hook_default_rules_link().
 */
function spam_detect_example_default_rules_link() {
  $items = array();
  $items['mark_not_spam'] = entity_import('rules_link', '{
    "settings" : {
      "text" : "Mark as not spam",
      "link_type" : "token",
      "bundles" : { "article" : "article", "page" : "page" },
      "entity_link" : 0
    },
    "name" : "mark_not_spam",
    "label" : "Mark as not spam",
    "path" : "not_spam",
    "entity_type" : "node",
    "rdf_mapping" : []
  }');
  $items['spam'] = entity_import('rules_link', '{
    "settings" : {
      "text" : "Mark as spam",
      "link_type" : "token",
      "bundles" : { "article" : "article", "page" : "page" },
      "entity_link" : 1
    },
    "name" : "spam",
    "label" : "Mark as spam",
    "path" : "spam",
    "entity_type" : "node",
    "rdf_mapping" : []
  }');
  return $items;
}
