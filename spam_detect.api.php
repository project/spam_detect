<?php

/**
 * @file
 * Describes Spam Detect's API for detecting spam.
 */

/**
 * Implements hook_spam_detect().
 *
 * @param string $text
 *   The text to evaluate for spam. Under normal operation, this should be a
 *   result of evaluation of tokens representing various fields on the
 *   submission.
 * @param array $context
 *   An associative array of keys and values to provide context about the text.
 *   All values are optional, so never assume any particular value will be
 *   present here unless you're forcing it in yourself.
 * @return float
 *   A value between 0 and 1, inclusive, corresponding to how sure the filter is
 *   that the text is spam. Filters can also return simple boolean values of
 *   FALSE and TRUE which correspond to values of 0 and 1, respectively. Filters
 *   may also return SPAM_DETECT_NO_OPINION if they do not wish to return a
 *   value to be counted to the text's spamminess score; for example, if the
 *   filter needs to contact an external API, but the server is not responding.
 */
function hook_spam_detect($text, $context) {
  // If the text has a percentage of non-ASCII characters greater than 20%,
  // consider it spam. (Obviously this won't work well for most languages
  // outside of western European ones.)
  $len = strlen($text);
  preg_match_all('/[^\x00-\x7f]/', $text, $matches);
  return count($matches[0]) / $len > .2;
}

/**
 * Implements hook_spam_detect_train_filter().
 *
 * In most contexts, this hook will be invoked after a human manually identifies
 * text as spammy or not spammy. It's intended that modules which can learn from
 * human intervention, such as Bayesian algorithm modules, will implement this
 * hook.
 *
 * @param string $text
 *   The text that you're training filters with.
 * @param boolk $is_spam
 *   Whether or not $text should be considered spammy.
 */
function hook_spam_detect_train_filter($text, $is_spam) {
  if ($is_spam) {
    // Teach myself that this text should be considered spammy.
  }
  else {
    // Teach myself that this text should not be considered spammy.
  }
}
