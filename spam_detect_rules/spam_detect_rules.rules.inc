<?php

/**
 * @file
 * Defines the condition class for Spam Detect.
 */

/**
 * Our condition class.
 */
class SpamDetectRulesCondition extends RulesConditionHandlerBase {
  /**
   * @inheritdoc
   */
  public static function getInfo() {
    $info = array(
      'name' => 'text_is_spammy',
      'label' => t('Text is spammy'),
      'help' => t(
        'Validates to TRUE if the text is determined to be spam by the enabled <a href="!path">Spam Detect filters and settings</a>.',
        array('!path' => url('admin/config/system/spam_detect'))
      ),
      'parameter' => array(
        'text' => array(
          'type' => 'text',
          'label' => t('Text'),
        ),
      ),
      'group' => t('Spam detect'),
    );
    return $info;
  }

  /**
   * @inheritdoc
   */
  public function execute($text) {
    return spam_detect_detect($text);
  }
}

/**
 * Our action class.
 */
class SpamDetectRulesAction extends RulesActionHandlerBase {
  /**
   * @inheritdoc
   */
  public static function getInfo() {
    $info = array(
      'name' => 'spam_detect_rules_train_filters',
      'label' => t('Train spam filters'),
      'help' => t('Trains any enabled spam filters that rely on user feedback to eventually recognize spam autonomously.'),
      'parameter' => array(
        'text' => array(
          'type' => 'text',
          'label' => t('Text to train filters with')
        ),
        'is_spam' => array(
          'type' => 'boolean',
          'label' => t('Should this text be considered spam?')
        ),
      ),
      'group' => t('Spam detect'),
    );
    return $info;
  }

  /**
   * @inheritdoc
   */
  public function execute($text, $is_spam) {
    spam_detect_train_filters($text, $is_spam);
  }
}
